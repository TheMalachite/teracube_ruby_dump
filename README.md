## full_v7101o-user 10 QP1A.190711.020 q0mp1t1v1001bsp release-keys
- Manufacturer: teracube
- Platform: mt6771
- Codename: Teracube_One
- Brand: Teracube
- Flavor: full_v7101o-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: q0mp1t1v1001bsp
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Teracube/Teracube_One/Teracube_One:10/QP1A.190711.020/q0mp1t1v1001bsp:user/release-keys
- OTA version: 
- Branch: full_v7101o-user-10-QP1A.190711.020-q0mp1t1v1001bsp-release-keys
- Repo: teracube_teracube_one_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
