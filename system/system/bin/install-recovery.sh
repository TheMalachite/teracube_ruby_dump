#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:15756588:b24ab47aa1a02476f9e72a002a0e0e1e258e856b; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:9389352:0b19b7823b4af2ce0e2c542df9e7b8b01b6c0e63 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:15756588:b24ab47aa1a02476f9e72a002a0e0e1e258e856b && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
